(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "e51e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/pages/Error404.vue?vue&type=template&id=4005bfbd&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"fullscreen bg-blue text-white text-center q-pa-md flex flex-center"},[_c('div',[_c('div',{staticStyle:{"font-size":"30vh"}},[_vm._v("\n      404\n    ")]),_c('div',{staticClass:"text-h2",staticStyle:{"opacity":".4"}},[_vm._v("\n      Oops. Nothing here...\n    ")]),_c('q-btn',{staticClass:"q-mt-xl",attrs:{"color":"white","text-color":"blue","unelevated":"","to":"/","label":"Go Home","no-caps":""}})],1)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/pages/Error404.vue?vue&type=template&id=4005bfbd&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/pages/Error404.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Error404vue_type_script_lang_js_ = ({
  name: 'Error404'
});
// CONCATENATED MODULE: ./src/pages/Error404.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_Error404vue_type_script_lang_js_ = (Error404vue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("2877");

// CONCATENATED MODULE: ./src/pages/Error404.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_Error404vue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Error404 = __webpack_exports__["default"] = (component.exports);

/***/ })

}]);